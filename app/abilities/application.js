import { Ability } from 'ember-can';

const IS_ELECTRON_ENV = !!(
  typeof window !== 'undefined' &&
  window.process &&
  window.process.type === 'renderer'
);

export default Ability.extend({
  canQuit: IS_ELECTRON_ENV
});
