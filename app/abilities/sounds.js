import Ember from 'ember';
import { Ability } from 'ember-can';
import { storageFor } from 'ember-local-storage';
import config from '../config/environment';

const { get, computed: { and } } = Ember;

export default Ability.extend({
  settings: storageFor('settings'),

  soundsEnabled: get(config, 'APP.soundsEnabled'),

  canPlayAmbient: and('soundsEnabled', 'settings.enableAmbientSounds'),
  canPlayKeyboard: and('soundsEnabled', 'settings.enableKeyboardSounds'),
  canPlayEffect: and('soundsEnabled', 'settings.enableEffectSounds')
});
