import Ember from 'ember';

const { Helper } = Ember;

export function eq([a, b]) {
  return a === b;
}

export default Helper.helper(eq);
