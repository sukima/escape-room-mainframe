import Ember from 'ember';

const {
  Component, get, getWithDefault,
  computed, computed: { and },
  inject: { service }
} = Ember;

export default Component.extend({
  animations: service(),
  tagName: 'span',
  classNames: ['passcode-field'],
  classNameBindings: ['hasFocus'],
  showCursor: true,
  hasFocus: true,
  size: 0,

  cursorEnabled: and('hasFocus', 'showCursor', 'animations.enabled'),

  paddedCode: computed('{code,size,cursorEnabled}', {
    get() {
      let paddedCode = getWithDefault(this, 'code', '').split('');
      let size = get(this, 'size');
      if (get(this, 'cursorEnabled') && paddedCode.length < size) {
        paddedCode.push('<span class="blink">&nbsp;</span>');
      }
      for (let i = paddedCode.length; i < size; i++) {
        paddedCode.push('&nbsp;');
      }
      return paddedCode.join('').htmlSafe();
    }
  })
});
