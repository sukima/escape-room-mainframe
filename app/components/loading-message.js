import Ember from 'ember';
import { task, timeout } from 'ember-concurrency';
import loadingMessages from '../utils/loading-messages';
import config from '../config/environment';

const { Component, get, set, } = Ember;
const { floor, random } = Math;

const MESSAGE_DELAY = get(config, 'APP.loadingMessageDelay');

export default Component.extend({
  tagName: '',

  showMessages: task(function * () {
    while (!(Ember.testing)) {
      let index = floor(random() * loadingMessages.length);
      set(this, 'message', loadingMessages[index]);
      yield timeout(MESSAGE_DELAY);
    }
  }).drop().on('init')
});
