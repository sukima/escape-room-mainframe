import Ember from 'ember';
import { CanMixin } from 'ember-can';

const {
  Component, get, tryInvoke,
  computed: { reads, not, or },
  inject: { service }
} = Ember;

export default Component.extend(CanMixin, {
  audio: service(),
  classNames: ['virtual-keyboard'],

  verifyLabel: 'Verify',

  verifyNotReady: not('verifyReady'),
  disableVerify: or('disabled', 'verifyNotReady'),
  disableDelete: reads('disabled'),

  actions: {
    keyPress(actionName, key) {
      let result = tryInvoke(this, actionName, key);
      if (this.can('play keyboard sounds')) {
        let soundName = get(this, 'disabled') || result === false ?
          'error' : 'key-press';
        get(this, 'audio').play(soundName);
      }
    }
  }
});
