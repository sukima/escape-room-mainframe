import Ember from 'ember';

const {
  Component, get,
  computed, computed: { gt },
  inject: { service }
} = Ember;
const { floor } = Math;

function pluralize(count, noun, suffix = 's') {
  if (count === 0) {
    return null;
  }
  return `${count} ${noun}${count !== 1 ? suffix : ''}`;
}

export default Component.extend({
  animations: service(),

  classNames: ['passcode-display'],

  isLocked: gt('lockoutTime', 0),

  humanizedTime: computed('lockoutTime', {
    get() {
      let timeLeft = get(this, 'lockoutTime');
      let minutes = floor(timeLeft / 61);
      let seconds = timeLeft % 61;
      return [
        pluralize(minutes, 'minute'),
        pluralize(seconds, 'second')
      ].compact().join(', ');
    }
  })
});
