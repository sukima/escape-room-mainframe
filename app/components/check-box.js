import Ember from 'ember';
import { CanMixin } from 'ember-can';

const { Component, get, inject: { service } } = Ember;

export default Component.extend(CanMixin, {
  audio: service(),
  
  tagName: 'button',
  classNames: ['button'],
  classNameBindings: ['checked:button--active'],
  attributeBindings: ['disable'],

  click() {
    if (get(this, 'disable')) {
      return false;
    }
    if (this.can('play effect sounds')) {
      get(this, 'audio').play('key-press');
    }
    get(this, 'update')(!get(this, 'checked'));
  }
});
