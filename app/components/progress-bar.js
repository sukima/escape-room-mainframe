import Ember from 'ember';
import { task, timeout } from 'ember-concurrency';
import config from '../config/environment';

const { Component, get, set, computed, tryInvoke } = Ember;

const STEPS = get(config, 'APP.progressSteps');
const STEP_DELAY = get(config, 'APP.progressStepDelay');

export default Component.extend({
  progressChars: computed('step', {
    get() {
      let chars = '';
      let step = get(this, 'step');
      for (let i = 0; i < STEPS; i++) {
        chars += (i <= step) ? '\u25c9' : '\u25ce';
      }
      return chars.htmlSafe();
    }
  }),

  timer: task(function * () {
    for (let i = 0; i < STEPS; i++) {
      set(this, 'step', i);
      yield timeout(STEP_DELAY);
    }
    tryInvoke(this, 'whenDone');
  }).restartable().on('init')
});
