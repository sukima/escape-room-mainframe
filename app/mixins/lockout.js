import Ember from 'ember';
import { task, timeout } from 'ember-concurrency';

const { Mixin, set } = Ember;

export const LOCKOUT_TIME = 120;

export default Mixin.create({
  lockout: task(function * () {
    for (let i = LOCKOUT_TIME; i >= 0; i--) {
      set(this, 'lockoutTimeLeft', i);
      yield timeout(1000);
    }
  }).drop()
});
