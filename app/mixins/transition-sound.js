import Ember from 'ember';
import { CanMixin } from 'ember-can';

const { Mixin, get, inject: { service } } = Ember;

export default Mixin.create(CanMixin, {
  audio: service(),

  transitionsSound: 'success',

  actions: {
    didTransition() {
      this._super(...arguments);
      if (this.can('play effect sounds')) {
        get(this, 'audio').play(get(this, 'transitionsSound'));
      }
    }
  }
});
