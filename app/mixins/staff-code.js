import Ember from 'ember';
import config from '../config/environment';

const { Mixin, get, set } = Ember;

const staffCode = get(config, 'APP.passCodes.staffCode');

export default Mixin.create({
  verifyStaffCode(key) {
    let index = get(this, 'staffCodeIndex');
    if (key === staffCode[index]) {
      index++;
    } else {
      index = 0;
    }
    if (index === staffCode.length) {
      this.send('staffCodeVerified');
      index = 0;
    }
    set(this, 'staffCodeIndex', index);
  }
});
