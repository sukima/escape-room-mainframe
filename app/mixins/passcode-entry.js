import Ember from 'ember';

const { Mixin, get, computed } = Ember;

export default Mixin.create({
  enteredCode: computed('keysBuffer.[]', {
    get() {
      return get(this, 'keysBuffer').join('');
    }
  }),

  addKey(key) {
    if (get(this, 'lockout.isIdle')) {
      get(this, 'keysBuffer').pushObject(key);
    }
  },

  popKey() {
    if (get(this, 'lockout.isIdle')) {
      get(this, 'keysBuffer').popObject();
    }
  },

  verify() {
    let validCode = get(this, 'validCode');
    let code = get(this, 'enteredCode');
    return validCode === code;
  }
});
