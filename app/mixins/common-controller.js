import Ember from 'ember';
import LockoutMixin from './lockout';
import PasscodeEntryMixin from './passcode-entry';
import StaffCodeMixin from './staff-code';
import { CanMixin } from 'ember-can';

const {
  Mixin, get, set, on, computed,
  computed: { notEmpty, reads },
  inject: { service },
  run: { next }
} = Ember;

const MIXINS = [CanMixin, StaffCodeMixin, LockoutMixin, PasscodeEntryMixin];

export default Mixin.create(...MIXINS, {
  audio: service(),
  animations: service(),

  codeSize: 10,

  verifyReady: notEmpty('enteredCode'),
  inputEnabled: reads('lockout.isIdle'),

  hasEnterdFullCode: computed('codeSize', 'enteredCode.length', {
    get() {
      return get(this, 'enteredCode.length') === get(this, 'codeSize');
    }
  }),

  reset: on('init', function () {
    get(this, 'lockout').cancelAll();
    set(this, 'lockoutTimeLeft', 0);
    set(this, 'staffCodeIndex', 0);
    set(this, 'keysBuffer', []);
  }),

  nextStep(routeName) {
    this.transitionToRoute(routeName);
    next(this, 'reset');
  },

  playSound(name) {
    if (this.can('play effect sounds')) {
      get(this, 'audio').play(name);
    }
  },

  actions: {
    staffCodeVerified() {
      this.nextStep('start');
    },

    registerKey(key) {
      this.verifyStaffCode(key);
      if (get(this, 'hasEnterdFullCode')) {
        return false;
      }
      if (get(this, 'inputEnabled')) {
        this.addKey(key);
      }
    },

    deleteKey() {
      set(this, 'staffCodeIndex', 0);
      if (get(this, 'inputEnabled')) {
        this.popKey();
      }
    },

    verifyCode() {
      set(this, 'staffCodeIndex', 0);
      if (!get(this, 'inputEnabled')) {
        return;
      }
      if (this.verify()) {
        this.nextStep(get(this, 'nextRoute'));
      } else {
        this.playSound('error');
        get(this, 'lockout').perform();
      }
    }
  }
});
