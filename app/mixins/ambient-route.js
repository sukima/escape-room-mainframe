import Ember from 'ember';
import { CanMixin } from 'ember-can';
import { task, timeout } from 'ember-concurrency';

const { Mixin, get, isPresent, inject: { service } } = Ember;
const { max, ceil, floor, random } = Math;

function randRange(min, max) {
  return floor(random() * (floor(max) - ceil(min) + 1)) + ceil(min);
}

export default Mixin.create(CanMixin, {
  audio: service(),

  minAbmientDelay: 10000,
  maxAbmientDelay: 90000,

  ambience: task(function * (soundFile) {
    if (!this.can('play ambient sounds')) {
      return;
    }
    const audio = get(this, 'audio');
    let minDelay = get(this, 'minAbmientDelay');
    let maxDelay = get(this, 'maxAbmientDelay');
    let duration = audio.durationFor(soundFile) || get(this, 'minAbmientDelay');
    if (minDelay > 0) {
      yield timeout(randRange(0, minDelay));
    }
    minDelay = max(minDelay, duration);
    while (true) {
      yield audio.playAmbient(soundFile);
      yield timeout(randRange(minDelay, maxDelay));
    }
  }).drop(),

  loopAmbiance: task(function * (soundFile) {
    if (!this.can('play ambient sounds')) {
      return;
    }
    const audio = get(this, 'audio');
    let duration = audio.durationFor(soundFile) || get(this, 'minAbmientDelay');
    while (true) {
      yield audio.playAmbient(soundFile);
      yield timeout(duration);
    }
  }).drop(),

  actions: {
    didTransition() {
      this._super(...arguments);
      let soundFile = get(this, 'ambientSound');
      if (isPresent(soundFile)) {
        let task = get(this, 'ambientLoop') ? 'loopAmbiance' : 'ambience';
        get(this, task).perform(soundFile);
      }
    },

    willTransition() {
      this._super(...arguments);
      get(this, 'ambience').cancelAll();
      get(this, 'loopAmbiance').cancelAll();
    }
  }
});
