import Ember from 'ember';
import { storageFor } from 'ember-local-storage';
import { CanMixin } from 'ember-can';

const {
  Controller, get, set, isNone, isBlank,
  inject: { service }
} = Ember;

export default Controller.extend(CanMixin, {
  audio: service(),
  settings: storageFor('settings'),
  passcodes: storageFor('passcodes'),

  inputSize: 10,

  actions: {
    selectInput(valueProp) {
      set(this, 'currentInput', valueProp);
      if (this.can('play effect sounds')) {
        get(this, 'audio').play('key-press');
      }
    },

    registerKey(key) {
      let currentInput = get(this, 'currentInput') || '';
      let inputValue = get(this, currentInput);
      let inputSize = get(this, 'inputSize');
      if (isBlank(currentInput) || inputValue.length === inputSize) {
        return false;
      }
      set(this, currentInput, `${inputValue}${key}`);
    },

    deleteKey() {
      let currentInput = get(this, 'currentInput');
      if (isNone(currentInput)) {
        return false;
      }
      let inputValue = get(this, currentInput);
      let end = inputValue.length - 1;
      set(this, currentInput, inputValue.substr(0, end));
    },

    navigateBack() {
      set(this, 'currentInput', null);
      this.transitionToRoute('start');
    },

    resetSettings() {
      if (this.can('play effect sounds')) {
        get(this, 'audio').play('key-beep');
      }
      get(this, 'settings').reset();
      get(this, 'passcodes').reset();
      set(this, 'currentInput', null);
    }
  }
});
