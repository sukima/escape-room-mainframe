/* globals requireNode */
import Ember from 'ember';
import { storageFor } from 'ember-local-storage';
import { CanMixin } from 'ember-can';
import config from '../config/environment';

const { Controller, get, inject: { service }, assert } = Ember;

export default Controller.extend(CanMixin, {
  audio: service(),
  settings: storageFor('settings'),
  author: config.APP.author,

  actions: {
    startGame() {
      if (this.can('play effect sounds')) {
        get(this, 'audio').play('success');
      }
      this.transitionToRoute('prompt1');
    },

    quitApp() {
      assert(
        '"quit application" not supported',
        this.can('quit application')
      );
      let app = requireNode('electron').remote.app;
      app.quit();
    }
  }
});
