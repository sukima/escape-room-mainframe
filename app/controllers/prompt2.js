import Ember from 'ember';
import CommonControllerMixin from '../mixins/common-controller';
import { storageFor } from 'ember-local-storage';

const { Controller, computed: { reads } } = Ember;

export default Controller.extend(CommonControllerMixin, {
  passcodes: storageFor('passcodes'),
  validCode: reads('passcodes.prompt2Code'),
  nextRoute: 'prompt3'
});
