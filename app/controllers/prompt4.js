import Ember from 'ember';
import CommonControllerMixin from '../mixins/common-controller';

const { Controller, get } = Ember;

export default Controller.extend(CommonControllerMixin, {
  nextRoute: 'prompt5',
  actions: {
    nextPrompt() {
      this.nextStep(get(this, 'nextRoute'));
    }
  }
});
