import Ember from 'ember';

const {
  Controller, get, computed,
  inject: { service }
} = Ember;

export default Controller.extend({
  animations: service(),

  showAnimations: computed('animations.{enabled,isActive}', 'currentRouteName', {
    get() {
      let currentRouteName = get(this, 'currentRouteName');
      return get(this, 'animations').enabledForRoute(currentRouteName);
    }
  })
});
