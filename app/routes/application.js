import Ember from 'ember';
import config from '../config/environment';
import { timeout } from 'ember-concurrency';

const {
  Route, get,
  inject: { service },
  RSVP: { all }
} = Ember;

const { APP: { audioFiles, startupDelay } } = config;

export default Route.extend({
  audio: service(),

  model() {
    const audio = get(this, 'audio');
    let promises = Object.keys(audioFiles).map(name => {
      let { file, duration } = audioFiles[name];
      return audio.load(name, file, duration);
    });
    return all([...promises, timeout(startupDelay)]);
  }
});
