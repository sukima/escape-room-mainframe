import Ember from 'ember';
import AmbiendRouteMixin from '../mixins/ambient-route';
import TransitionSoundMixin from '../mixins/transition-sound';

const { Route } = Ember;

export default Route.extend(TransitionSoundMixin, AmbiendRouteMixin, {
  ambientLoop: true,
  ambientSound: 'disconnected'
});
