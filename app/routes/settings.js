import Ember from 'ember';
import TransitionSoundMixin from '../mixins/transition-sound';

const { Route } = Ember;

export default Route.extend(TransitionSoundMixin);
