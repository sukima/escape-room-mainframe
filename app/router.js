import Ember from 'ember';
import config from './config/environment';

const Router = Ember.Router.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {
  this.route('start');
  this.route('settings');
  this.route('prompt1');
  this.route('prompt2');
  this.route('prompt3');
  this.route('prompt4');
  this.route('prompt5');
});

export default Router;
