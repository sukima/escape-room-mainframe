import StorageObject from 'ember-local-storage/local/object';

const Storage = StorageObject.extend();

Storage.reopenClass({
  initialState() {
    return {
      enableAmbientSounds: true,
      enableKeyboardSounds: true,
      enableEffectSounds: true,
      enableAnimations: true
    };
  }
});

export default Storage;
