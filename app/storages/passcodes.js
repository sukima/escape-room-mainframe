import StorageObject from 'ember-local-storage/local/object';
import config from '../config/environment';

const Storage = StorageObject.extend();

Storage.reopenClass({
  initialState() {
    return config.APP.passCodes;
  }
});

export default Storage;
