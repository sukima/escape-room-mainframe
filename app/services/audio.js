/* globals soundManager */
import Ember from 'ember';
import { task } from 'ember-concurrency';
import config from '../config/environment';

const {
  Service, get, set, isNone, isPresent, debug, assert,
  RSVP: { Promise }
} = Ember;

const SOUND_ENABLED = get(config, 'APP.soundsEnabled');

// soundManager can only be setup once. Sounds can only be created once.
// Memoize the promises. (This can happen in tests as new applications
// initialize).
let soundManagerPromise;

function setupSoundManager() {
  if (isNone(soundManagerPromise)) {
    debug(`Initializing SoundManager${SOUND_ENABLED ? '' : ' (disabled)'}`);
    soundManagerPromise = new Promise((resolve, reject) => {
      soundManager.setup({
        onready: resolve,
        ontimeout: reject
      });
    });
  }
  return soundManagerPromise;
}

function createSound(name, url) {
  return new Promise(resolve => {
    if (isPresent(soundManager.getSoundById(name))) {
      return resolve();
    }
    debug(`Pre-loading sound ${name} (${url})`);
    soundManager.createSound({
      url,
      id: name,
      autoLoad: true,
      onload: resolve
    });
  });
}

function playSound(sound) {
  debug(`Playing sound ${sound.id}`);
  return new Promise(resolve => {
    if (SOUND_ENABLED) {
      sound.play({onfinish: resolve});
    } else {
      resolve();
    }
  });
}

export default Service.extend({
  enqueueSound: task(function * (name) {
    yield get(this, '_manager');
    let sound = soundManager.getSoundById(name);
    assert(`Unknown sound ID '${name}'`, isPresent(sound));
    try {
      yield playSound(sound);
    } finally {
      sound.stop();
    }
  }).enqueue(),

  enqueueAmbient: task(function * (name) {
    yield get(this, 'enqueueSound').perform(name);
  }).restartable(),

  loadSound: task(function * (name, url) {
    yield get(this, '_manager');
    yield createSound(name, url);
  }),

  init() {
    this._super(...arguments);
    set(this, '_durations', new Map());
    set(this, '_manager', setupSoundManager());
  },

  durationFor(name) {
    return get(this, '_durations').get(name);
  },

  load(name, url, duration) {
    get(this, '_durations').set(name, duration);
    return get(this, 'loadSound').perform(name, url);
  },

  play(name) {
    get(this, 'enqueueAmbient').cancelAll();
    return get(this, 'enqueueSound').perform(name);
  },

  playAmbient(name) {
    return get(this, 'enqueueAmbient').perform(name);
  },

  stopAll() {
    return get(this, 'enqueueSound').cancelAll();
  }
});
