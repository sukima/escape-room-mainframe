import Ember from 'ember';
import { storageFor } from 'ember-local-storage';

const {
  Service, get,
  computed: { reads, not },
  inject: { service },
  String: { w }
} = Ember;

const POWERSAVE_ROUTES = w('start settings');

export default Service.extend({
  userIdle: service(),
  settings: storageFor('settings'),
  isActive: not('userIdle.isIdle'),
  enabled: reads('settings.enableAnimations'),

  enabledForRoute(name) {
    if (POWERSAVE_ROUTES.includes(name)) {
      return get(this, 'enabled') && get(this, 'isActive');
    } else {
      return get(this, 'enabled');
    }
  }
});
