import Ember from 'ember';
import { task, timeout } from 'ember-concurrency';

const { Service, get, set, computed } = Ember;
const { floor } = Math;

function pluralize(count, noun, suffix = 's') {
  if (count === 0) {
    return null;
  }
  return `${count} ${noun}${count !== 1 ? suffix : ''}`;
}

export const LOCKOUT_TIME = 120;

export default Service.extend({
  keysBuffer: [],
  lockoutTimeLeft: 0,

  lockout: task(function * () {
    for (let i = LOCKOUT_TIME; i > 0; i--) {
      set(this, 'lockoutTimeLeft', i);
      yield timeout(1000);
    }
  }).drop(),

  humanizedTimeLeft: computed('lockoutTimeLeft', {
    get() {
      let timeLeft = get(this, 'lockoutTimeLeft');
      let minutes = floor(timeLeft / 61);
      let seconds = timeLeft % 61;
      return [
        pluralize(minutes, 'minute'),
        pluralize(seconds, 'second')
      ].compact().join(', ');
    }
  }),

  enteredCode: computed('keysBuffer.[]', {
    get() {
      return get(this, 'keysBuffer').join('');
    }
  }),

  addKey(key) {
    if (get(this, 'lockout.isIdle')) {
      get(this, 'keysBuffer').pushObject(key);
    }
  },

  popKey() {
    if (get(this, 'lockout.isIdle')) {
      get(this, 'keysBuffer').popObject();
    }
  },

  verify() {
    let validCode = get(this, 'validCode');
    let code = get(this, 'enteredCode');
    if (validCode === code) {
      return true;
    } else {
      get(this, 'lockout').perform();
      return false;
    }
  },

  reset() {
    get(this, 'lockout').cancelAll();
    set(this, 'lockoutTimeLeft', 0);
    set(this, 'keysBuffer', []);
  }
});
