/* jshint node: true */
var fs = require('fs');
var path = require('path');

module.exports = function(environment) {
  var passCodes = JSON.parse(fs.readFileSync(path.join(__dirname, 'passcodes.json')));

  var ENV = {
    modulePrefix: 'escape-room-mainframe',
    environment: environment,
    rootURL: null,
    locationType: 'hash',
    EmberENV: {
      FEATURES: {
        // Here you can enable experimental features on an ember canary build
        // e.g. 'with-controller': true
      }
    },

    manifest: {
      enabled: (environment === 'production')
    },

    APP: {
      // Here you can pass flags/options to your application instance
      // when it is created
      author: {
        name: 'Devin Weaver',
        copyright: '2016',
        website: 'https://tritarget.org'
      },

      soundsEnabled: true,
      startupDelay: 2500,
      loadingMessageDelay: 3000,
      progressSteps: 20,
      progressStepDelay: 200,

      passCodes: passCodes[environment],

      audioFiles: {
        'alarm':        {file: 'audio/alarm.mp3',        duration: 50000},
        'beep-error':   {file: 'audio/beep-error.mp3',   duration: 1000},
        'disconnected': {file: 'audio/disconnected.mp3', duration: 3000},
        'error':        {file: 'audio/error.mp3',        duration: 340},
        'key-press':    {file: 'audio/key-press.mp3',    duration: 157},
        'success':      {file: 'audio/success.mp3',      duration: 672}
      }
    }
  };

  if (environment === 'development') {
    // ENV.APP.LOG_RESOLVER = true;
    // ENV.APP.LOG_ACTIVE_GENERATION = true;
    // ENV.APP.LOG_TRANSITIONS = true;
    // ENV.APP.LOG_TRANSITIONS_INTERNAL = true;
    // ENV.APP.LOG_VIEW_LOOKUPS = true;
  }

  if (environment === 'test') {
    // Testem prefers this...
    ENV.locationType = 'none';

    // keep test console output quieter
    ENV.APP.LOG_ACTIVE_GENERATION = false;
    ENV.APP.LOG_VIEW_LOOKUPS = false;

    ENV.APP.rootElement = '#ember-testing';

    ENV.APP.soundsEnabled = false;
    ENV.APP.startupDelay = 0;
    ENV.APP.progressSteps = 2;
    ENV.APP.progressStepDelay = 600;
  }

  if (environment === 'staging') {
    // This is to be used on a kiosk. No history!
    ENV.rootURL = '/mainframe/';
    ENV.locationType = 'none';
  }

  if (environment === 'production') {
    ENV.rootURL = null;
  }

  return ENV;
};
