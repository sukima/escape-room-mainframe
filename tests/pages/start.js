import { create, clickable, visitable } from 'ember-cli-page-object';

export default create({
  visit: visitable('/start'),
  startGame: clickable('button.action-start'),
  settings: clickable('button.action-settings')
});
