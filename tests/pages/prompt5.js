import { create, visitable } from 'ember-cli-page-object';
import { virtualKeyboard } from './components/virtual-keyboard';

export default create({
  visit: visitable('/prompt5'),
  keyboard: virtualKeyboard
});
