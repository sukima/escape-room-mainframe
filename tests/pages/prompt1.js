import { create, visitable } from 'ember-cli-page-object';
import { virtualKeyboard } from './components/virtual-keyboard';
import { passcodeDisplay } from './components/passcode-display';

export default create({
  visit: visitable('/prompt1'),
  passcodeDisplay: passcodeDisplay,
  keyboard: virtualKeyboard
});
