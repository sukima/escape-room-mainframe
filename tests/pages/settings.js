import {
  create, text, collection, hasClass, notHasClass, visitable
} from 'ember-cli-page-object';
import { virtualKeyboard } from './components/virtual-keyboard';

export default create({
  visit: visitable('/settings'),
  keyboard: virtualKeyboard,
  sounds: {
    scope: '.panel .panel__inner--left',
    buttons: collection({
      itemScope: 'button',
      item: {
        enabled: hasClass('button--active'),
        disabled: notHasClass('button--active')
      }
    })
  },
  passcodes: collection({
    scope: '.panel .panel__inner--right',
    itemScope: '.passcode-field',
    item: {
      hasFocus: hasClass('has-focus'),
      value: text('.code')
    }
  })
});
