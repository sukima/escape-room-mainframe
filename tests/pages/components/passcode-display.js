import PageObject from 'ember-cli-page-object';

const { hasClass }  = PageObject;

export const passcodeDisplay = {
  scope: '.passcode-display',
  code: {
    scope: '.passcode-display__code',
    isError: hasClass('passcode-display--error')
  },
  lockoutMessage: {scope: '.passcode-display__error-message'}
};

export default PageObject.create(passcodeDisplay);
