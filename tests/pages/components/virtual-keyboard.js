import { create, clickOnText, clickable, collection } from 'ember-cli-page-object';

export const virtualKeyboard = {
  scope: '.virtual-keyboard',
  buttons: collection({itemScope: '.virtual-keyboard__key'}),
  press: clickOnText('.virtual-keyboard__key--letter'),
  clear: clickable('.virtual-keyboard__key--delete'),
  verify: clickable('.virtual-keyboard__key--verify'),
  enterCode(code) {
    for (let buttonText of code.split('')) {
      this.press(buttonText);
    }
    return this;
  }
};

export default create(virtualKeyboard);
