import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';
import page from '../../pages/components/virtual-keyboard';

moduleForComponent('virtual-keyboard', 'Integration | Component | virtual keyboard', {
  integration: true,
  beforeEach() {
    page.setContext(this);
  },
  afterEach() {
    page.removeContext();
  }
});

test('it renders buttons', function(assert) {
  page.render(hbs`{{virtual-keyboard}}`);
  assert.ok(page.buttons().count > 0, 'expected buttons to be rendered');
});

