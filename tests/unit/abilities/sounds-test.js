import Ember from 'ember';
import { moduleFor, test } from 'ember-qunit';

const { get, typeOf, String: { w } } = Ember;

const SOUND_ABILITIES = w(`
  canPlayAmbient
  canPlayKeyboard
  canPlayEffect
`.trim());

moduleFor('ability:sounds', 'Unit | Ability | sound', {
  // Specify the other units that are required for this test.
  needs: ['storage:settings']
});

// Replace this with your real tests.
test('has abilities for different types of sounds', function(assert) {
  var ability = this.subject();
  for (let prop of SOUND_ABILITIES) {
    let result = get(ability, prop);
    assert.equal(typeOf(result), 'boolean');
  }
});
