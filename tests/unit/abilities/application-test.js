import Ember from 'ember';
import { moduleFor, test } from 'ember-qunit';

const { get, typeOf } = Ember;

moduleFor('ability:application', 'Unit | Ability | application', {
  // Specify the other units that are required for this test.
  // needs: ['service:foo']
});

test('it provids a boolean for "can quit application"', function(assert) {
  let ability = this.subject();
  let result = get(ability, 'canQuit');
  assert.equal(typeOf(result), 'boolean');
});
