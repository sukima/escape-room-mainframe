import Ember from 'ember';
import { test } from 'qunit';
import moduleForAcceptance from 'escape-room-mainframe/tests/helpers/module-for-acceptance';
import page from '../pages/prompt4';
import config from 'escape-room-mainframe/config/environment';

const { get, run: { cancelTimers } } = Ember;

const RENDER_DELAY = 500;
const STAFF_CODE = get(config, 'APP.passCodes.staffCode');
const SUCCESS_ROUTE_NAME = 'prompt5';
const RESET_ROUTE_NAME = 'start';

moduleForAcceptance('Acceptance | Route | Prompt 4', {
  beforeEach() {
    window.localStorage.clear();
  }
});

test('moves to the next step when loading timer is complete', function (assert) {
  page.visit();
  andThen(() => {
    assert.equal(currentRouteName(), SUCCESS_ROUTE_NAME);
  });
});

test('resets with staff code', function (assert) {
  page.visit();
  setTimeout(() => {
    cancelTimers();
    page.keyboard.enterCode(STAFF_CODE);
    andThen(() => {
      assert.equal(currentRouteName(), RESET_ROUTE_NAME);
    });
  }, RENDER_DELAY);
});
