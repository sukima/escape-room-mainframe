import Ember from 'ember';
import { test } from 'qunit';
import moduleForAcceptance from 'escape-room-mainframe/tests/helpers/module-for-acceptance';
import config from 'escape-room-mainframe/config/environment';
import page from '../pages/settings';

const { get } = Ember;

const FIRST_INPUT_DEFAULT_VALUE = get(config, 'APP.passCodes.staffCode');

moduleForAcceptance('Acceptance | Route | Settings', {
  beforeEach() {
    window.localStorage.clear();
  }
});

test('can adjust sound settings', function (assert) {
  let initialState;
  page.visit();
  andThen(() => {
    initialState = page.sounds.buttons().map(button => button.enabled);
    page.sounds.buttons().forEach(button => button.click());
  });
  andThen(() => {
    let expectedState = initialState.map(state => !state);
    let newState = page.sounds.buttons().map(button => button.enabled);
    assert.deepEqual(newState, expectedState);
    page.sounds.buttons().forEach(button => button.click());
  });
  andThen(() => {
    let newState = page.sounds.buttons().map(button => button.enabled);
    assert.deepEqual(newState, initialState);
  });
});

test('can adjust passcodes', function (assert) {
  page.visit();
  page.passcodes(0).click();
  andThen(() => {
    assert.ok(page.passcodes(0).hasFocus, 'expected first passcode to have focus');
  });
  page.keyboard.enterCode('ABC');
  andThen(() => {
    assert.equal(page.passcodes(0).value, `${FIRST_INPUT_DEFAULT_VALUE}ABC`);
  });
});

test('can return to start route', function (assert) {
  page.visit();
  page.keyboard.verify();
  andThen(() => {
    assert.equal(currentRouteName(), 'start');
  });
});
