import Ember from 'ember';
import { test } from 'qunit';
import moduleForAcceptance from 'escape-room-mainframe/tests/helpers/module-for-acceptance';
import page from '../pages/prompt3';
import config from 'escape-room-mainframe/config/environment';

const { get, run: { cancelTimers } } = Ember;

const RENDER_DELAY = 500;
const BAD_PASSCODE = 'BAD';
const GOOD_PASSCODE = get(config, 'APP.passCodes.prompt3Code');
const STAFF_CODE = get(config, 'APP.passCodes.staffCode');
const SUCCESS_ROUTE_NAME = 'prompt4';
const RESET_ROUTE_NAME = 'start';

moduleForAcceptance('Acceptance | Route | Prompt 3', {
  beforeEach() {
    window.localStorage.clear();
  }
});

test('starts a lockout when wrong code is entered', function (assert) {
  let done = assert.async();

  page.visit();
  page.keyboard.enterCode(BAD_PASSCODE);
  page.keyboard.verify();

  setTimeout(() => {
    assert.ok(
      page.passcodeDisplay.lockoutMessage.isVisible,
      'expected lockout message to be visible'
    );
    cancelTimers();
    done();
  }, RENDER_DELAY);
});

test('moves to the next step when correct code is entered', function (assert) {
  let done = assert.async();

  page.visit();
  page.keyboard.enterCode(GOOD_PASSCODE);
  page.keyboard.verify();

  setTimeout(() => {
    assert.equal(currentRouteName(), SUCCESS_ROUTE_NAME);
    cancelTimers();
    done();
  }, RENDER_DELAY);
});

test('resets with staff code', function (assert) {
  page.visit();
  page.keyboard.enterCode(STAFF_CODE);
  andThen(() => {
    assert.equal(currentRouteName(), RESET_ROUTE_NAME);
  });
});
