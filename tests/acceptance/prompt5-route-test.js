import Ember from 'ember';
import { test } from 'qunit';
import moduleForAcceptance from 'escape-room-mainframe/tests/helpers/module-for-acceptance';
import page from '../pages/prompt5';
import config from 'escape-room-mainframe/config/environment';

const { get } = Ember;

const STAFF_CODE = get(config, 'APP.passCodes.staffCode');
const RESET_ROUTE_NAME = 'start';

moduleForAcceptance('Acceptance | Route | Prompt 5', {
  beforeEach() {
    window.localStorage.clear();
  }
});

test('resets with staff code', function (assert) {
  page.visit();
  page.keyboard.enterCode(STAFF_CODE);
  andThen(() => {
    assert.equal(currentRouteName(), RESET_ROUTE_NAME);
  });
});
