import { test } from 'qunit';
import moduleForAcceptance from 'escape-room-mainframe/tests/helpers/module-for-acceptance';
import page from '../pages/start';

moduleForAcceptance('Acceptance | Route | Start');

test('opens settings when settings button is pressed', function (assert) {
  page.visit();
  page.settings();
  andThen(() => {
    assert.equal(currentRouteName(), 'settings');
  });
});

test('starts the game when start button is pressed', function (assert) {
  page.visit();
  page.startGame();
  andThen(() => {
    assert.equal(currentRouteName(), 'prompt1');
  });
});
